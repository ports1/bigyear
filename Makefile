#
# The "install" target ("make install") will copy the bigyear binary to 
# the /usr/local/bin directory on the local system.  
#
# The "clean" target ("make clean") will remove the 
# object files, and the bigyear binary.
#

# CC = gcc
# CFLAGS = -s -Wall -pedantic
CC = cc
CFLAGS = 

main :	bigyear

all :	bigyear

bigyear : bigyear.c
	$(CC) $(CFLAGS) -o bigyear bigyear.c 

install :
	cp bigyear /usr/local/bin/
	cp bigyear.1 /usr/local/man/man1

clean :
	rm -f *.o bigyear

